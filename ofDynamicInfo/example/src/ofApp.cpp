#include "ofApp.h"
#include "ofxDynamicInfo.h"

//--------------------------------------------------------------
void ofApp::setup(){
	
	setupDynSample();

}

//--------------------------------------------------------------
void ofApp::update(){
	
	for (int i = 0; i < Samps.size(); i++)
	{
		Samps[i]->update();
	}
	
}
//--------------------------------------------------------------
void ofApp::draw(){
	for (int i = 0; i < Samps.size(); i++)
	{
	Samps[i]->draw();
	}	
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	
	if (key == 'p')
	{
		Samps.clear();
		setupDynSample();
	}


}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

	
}
//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
	
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

void ofApp::setupDynSample()
{
	
	dx = ofGetWidth()/9;
	dy = ofGetHeight()/6;

	for (int i = 1; i < 9; i++)
	{
		for (int h = 1; h <6 ;h++)
		{
			
			ofPoint p;
			p.x = dx*i;
			p.y = dy*h;
			Poss.push_back(p);
		}
		
	}

	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->staticText("static",Poss[0]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->staticImage("img1.jpg",Poss[1]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->sraticImageInfo("sratic","img2.jpg",Poss[2]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->staticRectInfo("static",Poss[3]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->staticDialogInfo("static",Poss[4],"text1.ttf",10,ofColor::red);
	Samps.push_back(pSamp);

	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeInText(5,"fadeIn",Poss[5]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeInImage(5,"img1.jpg",Poss[6]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeInImageInfo(5,"fadeIn","img2.jpg",Poss[7]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeInRectInfo(5,"fadeIn",Poss[8],"text1.ttf",10);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeInDialogInfo(5,"fadeIn",Poss[9]);
	Samps.push_back(pSamp);
	
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeOutText(5,"fadeOut",Poss[10]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeOutImage(5,"img1.jpg",Poss[11]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeOutImageInfo(5,"fadeOut","img2.jpg",Poss[12]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeOutRectInfo(5,"fadeOut",Poss[13],"text1.ttf",10);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeOutDialogInfo(5,"fadeOut",Poss[14]);
	Samps.push_back(pSamp);
	
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rotateText(360*2,5,"rotate",Poss[15],"text2.ttf",20,ofColor::red);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rotateImage(360*2,5,"img1.jpg",Poss[16]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rotateImageInfo(360*2,5,"rotate","img2.jpg",Poss[17]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rotateRectInfo(360*2,5,"rotate",Poss[18],"text1.ttf",10,ofColor::green);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rotateDialogInfo(360*2,5,"rotate",Poss[19]);
	Samps.push_back(pSamp);
	
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleInText(5,"fadeScaleIn",Poss[20]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleInImage(5,"img1.jpg",Poss[21]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleInImageInfo(5,"fadeScaleIn","img2.jpg",Poss[22]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleInRectInfo(5,"fadeScaleIn",Poss[23],"text1.ttf",10);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleInDialogInfo(5,"fadeScaleIn",Poss[24]);
	Samps.push_back(pSamp);
	
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleOutText(5,"fadeScaleOut",Poss[25]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleOutImage(5,"img1.jpg",Poss[26]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleOutImageInfo(5,"fadeScaleOut","img2.jpg",Poss[27]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleOutRectInfo(5,"fadeScaleOut",Poss[28],"text1.ttf",10);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeScaleOutDialogInfo(5,"fadeScaleOut",Poss[29]);
	Samps.push_back(pSamp);
	
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeShakeText(5,"fadeShake",Poss[30]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeShakeImage(5,"img1.jpg",Poss[31]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeShakeImageInfo(5,"fadeShake","img2.jpg",Poss[32]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeShakeRectInfo(5,"fadeShake",Poss[33],"text1.ttf",10);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->fadeShakeDialogInfo(5,"fadeShake",Poss[34]);
	Samps.push_back(pSamp);

	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rollText(30,1,"roll",Poss[35]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rollImage(30,1,"img1.jpg",Poss[36],false);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rollImageInfo(30,1,"roll","img2.jpg",Poss[37]);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rollRectInfo(30,1,"roll",Poss[38],false);
	Samps.push_back(pSamp);
	pSamp.reset(new ofxDynamicInfoSample);
	pSamp->rollDialogInfo(30,1,"roll",Poss[39]);
	Samps.push_back(pSamp);
}