#pragma once

#include "ofMain.h"
#include "ofxDynamicInfoGroup.h"
#include "ofxDynamicInfoSample.h"

class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void setupDynSample();

		//sample
		vector<ofPtr<ofxDynamicInfoSample>> Samps;
		ofPtr<ofxDynamicInfoSample> pSamp;
		vector<ofPoint> Poss;
		float dx,dy;

		ofPtr<ofxShake> pShake;
		ofPtr<ofxImageThing> pImg;
		ofPtr<ofxDynamicInfo> pDyn;
	
};
